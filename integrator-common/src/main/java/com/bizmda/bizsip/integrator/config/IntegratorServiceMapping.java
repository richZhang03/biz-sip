package com.bizmda.bizsip.integrator.config;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.io.resource.NoResourceException;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.integrator.executor.*;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * @author 史正烨
 */
@Slf4j
public class IntegratorServiceMapping {
    private Map<String, AbstractIntegratorExecutor> integratorExecutorMap;
    //    private Map<String, SpringBeanIntegratorExecutor> prefixServiceIntegratorExecutorMap;
    private String configPath;
    private boolean isSpringBeanLoad = false;
//    private Map<String, JavaIntegratorServiceInterface> javaIntegratorServiceMap = null;

    private static final Map<String, Object> SERVICE_SCRIPT_SUFFIX_MAP = new HashMap<>();

    static {
        SERVICE_SCRIPT_SUFFIX_MAP.put("script", ScriptIntegratorExecutor.class);
    }

    public IntegratorServiceMapping(String configPath) throws BizException {
        this.configPath = configPath;
        this.load();
    }

    public void load() throws BizException {
        String scriptPath = null;
        this.integratorExecutorMap = new HashMap<>(16);
        List<File> files = new ArrayList<>();
        if (this.configPath == null) {
            try {
                ClassPathResource resource = new ClassPathResource("/service");
                File fileDir = resource.getFile();
                scriptPath = fileDir.getPath();
                if (fileDir.exists() && fileDir.isDirectory()) {
                    files = FileUtil.loopFiles(fileDir);
                }
            } catch (NoResourceException e) {
                log.warn("资源为空:/service");
            }
        } else {
            if (this.configPath.endsWith("/")) {
                scriptPath = this.configPath + "service";
            } else {
                scriptPath = this.configPath + "/service";
            }
            files = FileUtil.loopFiles(scriptPath);
        }
        String suffix;
        AbstractIntegratorExecutor integratorService = null;

        for (File file : files) {
            suffix = FileUtil.getSuffix(file).toLowerCase();
            Class<AbstractIntegratorExecutor> integratorClazz = (Class<AbstractIntegratorExecutor>) SERVICE_SCRIPT_SUFFIX_MAP.get(suffix);
            if (integratorClazz != null) {
                FileReader fileReader = new FileReader(file);
                String fileContent = fileReader.readString();
                String allPath = FileUtil.normalize(file.getPath());
                String serviceId = allPath.substring(scriptPath.length(), allPath.length() - suffix.length() - 1);
                log.info("装载聚合服务:{}", serviceId);
                try {
                    Constructor<AbstractIntegratorExecutor> constructor = integratorClazz.getDeclaredConstructor(String.class, String.class, Map.class);
                    Map configMap = new HashMap(16);
                    configMap.put("script", fileContent);
                    integratorService = constructor.newInstance(serviceId, suffix, configMap);
                } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                    throw new BizException(BizResultEnum.INTEGRATOR_SERVICE_CLASS_LOAD_ERROR, e);
                }
                integratorService.init();
                integratorExecutorMap.put(serviceId, integratorService);
            }
        }
    }

    private void loadServiceFromYml() throws BizException {
        Yaml yaml = new Yaml();
        List<Map<String, Object>> springbeanServiceList = null;
        try {
            if (this.configPath == null) {
                ClassPathResource resource = new ClassPathResource("/service.yml");
                springbeanServiceList = (List<Map<String, Object>>) yaml.load(new FileInputStream(resource.getFile()));
            } else {
                springbeanServiceList = (List<Map<String, Object>>) yaml.load(new FileInputStream(new File(this.configPath + "/service.yml")));
            }
        } catch (FileNotFoundException e) {
            throw new BizException(BizResultEnum.OTHER_FILE_NOTFOUND, "service.yml");
        }
        for (Map<String, Object> serviceConfigMap : springbeanServiceList) {
            String bizServiceId = (String) serviceConfigMap.get("bizServiceId");
            String type = (String) serviceConfigMap.get("type");
            log.info("装载聚合服务:" + bizServiceId + "  类型:" + type);
            switch (type) {
                case "bean-service":
                    BeanIntegratorExecutor beanIntegratorExecutor = new BeanIntegratorExecutor(bizServiceId, type, serviceConfigMap);
                    beanIntegratorExecutor.init();
                    this.integratorExecutorMap.put(bizServiceId, beanIntegratorExecutor);
                    break;
                case "app-bean-service":
                    AppBeanIntegratorExecutor appBeanIntegratorExecutor = new AppBeanIntegratorExecutor(bizServiceId, type, serviceConfigMap);
                    appBeanIntegratorExecutor.init();
                    this.integratorExecutorMap.put(bizServiceId, appBeanIntegratorExecutor);
                    break;
                case "integrator-bean-service":
                    IntegratorBeanIntegratorExecutor integratorBeanIntegratorExecutor = new IntegratorBeanIntegratorExecutor(bizServiceId, type, serviceConfigMap);
                    integratorBeanIntegratorExecutor.init();
                    this.integratorExecutorMap.put(bizServiceId, integratorBeanIntegratorExecutor);
                    break;
                case "sink-service":
                    SinkServiceIntegratorExecutor serviceIntegratorExecutor = new SinkServiceIntegratorExecutor(bizServiceId, type, serviceConfigMap);
                    serviceIntegratorExecutor.init();
                    this.integratorExecutorMap.put(bizServiceId, serviceIntegratorExecutor);
                    break;
                default:
                    log.error("装载出错，未知服务类型:" + type);
            }
        }
    }

    public AbstractIntegratorExecutor getIntegratorService(String bizServiceId) {
        if (!this.isSpringBeanLoad) {
            this.isSpringBeanLoad = true;
            try {
                this.loadServiceFromYml();
            } catch (BizException e) {
                log.error("装载service.yml出错", e);
            }
        }

        AbstractIntegratorExecutor abstractIntegratorExecutor = this.integratorExecutorMap.get(bizServiceId);

        return abstractIntegratorExecutor;
    }
}
