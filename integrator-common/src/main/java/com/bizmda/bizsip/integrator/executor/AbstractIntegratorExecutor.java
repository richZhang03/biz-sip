package com.bizmda.bizsip.integrator.executor;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import lombok.Data;

import java.util.Map;

/**
 * @author 史正烨
 */
@Data
public abstract class AbstractIntegratorExecutor {
    private String serviceId;
    private String type;
    private Map configMap;
    protected AbstractIntegratorExecutor() {
    }

    protected AbstractIntegratorExecutor(String serviceId, String type, Map configMap) {
        this.serviceId = serviceId;
        this.type = type;
        this.configMap = configMap;
    }

    /**
     * 服务整合器初始化
     */
    public abstract void init();

    /**
     * 调用聚合服务
     * @param message 传入的消息
     * @return 返回的消息
     */
    public abstract BizMessage<JSONObject> doBizService(BizMessage<JSONObject> message) throws BizException;
}
