package com.bizmda.bizsip.integrator.executor;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.*;
import lombok.extern.slf4j.Slf4j;
import org.ssssssss.script.MagicScriptContext;

import java.util.Map;

/**
 * @author 史正烨
 */
@Slf4j
public class ScriptIntegratorExecutor extends AbstractIntegratorExecutor {
    private String content;


    public ScriptIntegratorExecutor(String serviceId, String type, Map configMap) {
        super(serviceId, type, configMap);
        this.content = (String) configMap.get("script");
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doBizService(BizMessage inBizMessage) throws BizException {
        BizUtils.debug("入参",inBizMessage);
        MagicScriptContext context = new MagicScriptContext();
        context.set("bizmessage", inBizMessage);
        log.trace("执行script:\n{}",this.content);
        Object result = MagicScriptHelper.executeScript(this.content, context);
        if (result instanceof ExecutorError) {
            if (((ExecutorError)result).isTimeoutException()) {
                log.warn("sip.timeout():{}", ((ExecutorError) result).getMessage());
                throw new BizTimeOutException(((ExecutorError) result).getMessage());
            }
            else {
                log.warn("sip.error():{}", ((ExecutorError) result).getMessage());
                throw new BizException(BizResultEnum.INTEGRATOR_SCRIPT_RETURN_EXECUTOR_ERROR, ((ExecutorError) result).getMessage());
            }
        }

        log.debug("script返回成功");
        BizUtils.trace("脚本引擎返回结果", result);
        BizMessage<JSONObject> result1 = BizMessage.buildJsonObjectMessage(inBizMessage,result);
        BizUtils.debug("返回",result1);
        return result1;
    }

}
