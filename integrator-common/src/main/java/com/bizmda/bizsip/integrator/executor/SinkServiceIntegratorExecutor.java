package com.bizmda.bizsip.integrator.executor;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.integrator.service.SipService;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class SinkServiceIntegratorExecutor extends AbstractIntegratorExecutor {
    private String sinkId;
    private SipService sipService;

    public SinkServiceIntegratorExecutor(String serviceId, String type, Map configMap) {
        super(serviceId, type, configMap);
        this.sinkId = (String)configMap.get("sinkId");
        this.sipService = SpringUtil.getBean(SipService.class);
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doBizService(BizMessage<JSONObject> message) throws BizException {
        BizUtils.debug("入参",message);
        BizMessage<JSONObject> result = this.sipService.callSink(this.sinkId,message.getData());
        BizUtils.debug("函数返回",result);
        return result;
    }
}
