package com.bizmda.bizsip.integrator.config;

import com.bizmda.bizsip.common.BizConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Slf4j
@Configuration
public class RabbitmqConfig {

    public static final String DELAY_SERVICE_EXCHANGE = "exchange.direct.bizsip.delayservice";
    public static final String DELAY_SERVICE_QUEUE = "queue.delay.bizsip.delayservice";
    public static final String DELAY_SERVICE_ROUTING_KEY = "key.bizsip.delayservice";
//    public static final String MQ_SERVER_EXCHANGE = "exchange.dircect.bizsip.mqserver";

    @Autowired
    private CachingConnectionFactory connectionFactory;
//
//    @Bean
//    public DirectExchange delayServiceExchange(){
//        DirectExchange exchange = new DirectExchange(DELAY_SERVICE_EXCHANGE, true, false);
//        exchange.setDelayed(true);
//        return exchange;
//    }
//
//    @Bean
//    public Queue delayServiceQueue(){
//        return new Queue(DELAY_SERVICE_QUEUE,true);
//    }
//
//    @Bean
//    public Binding delayServiceBinding(){
//        return BindingBuilder.bind(delayServiceQueue()).to(delayServiceExchange()).with(DELAY_SERVICE_ROUTING_KEY);
//    }

//    @Bean
//    public DirectExchange mqServerExchange(){
//        DirectExchange exchange = new DirectExchange(MQ_SERVER_EXCHANGE, true, false);
//        return exchange;
//    }

    @Bean
    public DirectExchange logExchange(){
        DirectExchange exchange = new DirectExchange(BizConstant.BIZSIP_LOG_EXCHANGE, true, false);
        return exchange;
    }


//    /**
//     * 多个消费者
//     * @return
//     */
//    @Bean(name = "multiListenerContainer")
//    public SimpleRabbitListenerContainerFactory multiListenerContainer(Environment env,
//                                                                       SimpleRabbitListenerContainerFactoryConfigurer factoryConfigurer){
//        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
//        factoryConfigurer.configure(factory,connectionFactory);
//        factory.setMessageConverter(new Jackson2JsonMessageConverter());
//        //确认消费模式-NONE
//        factory.setAcknowledgeMode(AcknowledgeMode.NONE);
//        factory.setConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.concurrency",int.class));
//        factory.setMaxConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.max-concurrency",int.class));
//        factory.setPrefetchCount(env.getProperty("spring.rabbitmq.listener.simple.prefetch",int.class));
//        return factory;
//    }

    @Bean
    public RabbitTemplate rabbitTemplate(){
//        connectionFactory.setPublisherConfirms(true);
//        connectionFactory.setPublisherReturns(true);
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
//        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());

//        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
//            @Override
//            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
//                // TODO
//            }
//        });
//        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
//            @Override
//            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
//                log.debug("消息丢失:exchange({}),route({}),replyCode({}),replyText({}),message:{}",exchange,routingKey,replyCode,replyText,message);
//            }
//        });
        return rabbitTemplate;
    }
}
