#!/bin/sh
set -v on
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/sample1" -X POST --data '{"accountNo":"62001818","sex":"0","email":"123232@163.com","mobile":"18601872345"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/sample2" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
#curl -H "Content-Type:application/json" -X POST --data '{"serviceId":"/openapi/sample2","accountNo":"003"}' http://localhost:8080/source1|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/sample4" -X POST --data '{"accountNo":"005"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/sample5" -X POST --data '{"accountName": "王五","balance": 500,"accountNo":"005"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/server4" -X POST --data '{"accountName": "王五","sex": "0","accountNo":"005"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/server4" -X POST --data '{"accountName": "王五","sex": "1","accountNo":"005"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/server4" -X POST --data '{"accountName": "王五","sex": "2","accountNo":"005"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/server5" -X POST --data '{"accountName": "王五","sex": "0","accountNo":"005","balance":100}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/server5" -X POST --data '{"accountName": "王五","sex": "0","accountNo":"005","balance":1000}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/server5" -X POST --data '{"accountName": "王五","sex": "2","accountNo":"005","balance":1000}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/server6" -X POST --data '{"accountName": "王五","balance": 500,"accountNo":"005","sex":"0"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/server7" -X POST --data '{"accountName": "王五","balance": 500,"accountNo":"005","sex":"0"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" http://localhost:8080/sink13|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/db1" -X POST --data '{"accountNo":"002"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/redis1" -X POST --data '{"accountNo":"002"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/safservice" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:sample1.service1" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sample/service2" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/server10" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sample/service3" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sample/service4" -X POST --data '{"maxRetryNum":2,"result":"success"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/springbean" -X POST --data '{"methodName":"doService1","params":["003"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/springbean" -X POST --data '{"methodName":"doService2","params":["003",1]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/springbean" -X POST --data '{"methodName":"queryCustomerDTO","params":["003"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sinkService" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sample/service5" -X POST --data '{"method":"notify","maxRetryNum":3,"result":"success"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/sink14" -X POST --data '{"accountName": "王五","sex": "0","accountNo":"005","balance":100}' http://localhost:8888/api|jq
curl -H "Content-Type:application/xml" -X POST --data '<?xml version="1.0" encoding="UTF-8" standalone="no"?><root><accountName>王五</accountName><balance>500</balance><accountNo>005</accountNo></root>' http://localhost:8080/source4
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/openapi/sink15" -X POST --data '{"accountName": "王五","sex": "0","accountNo":"005","balance":100}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "service-id:/openapi/sample2" -X POST --data '{"accountNo":"003"}' http://localhost:8081/rest|jq
curl -H "Content-Type:application/json" -X POST --data '{"accountNo":"003"}' http://localhost:8081/rest|jq