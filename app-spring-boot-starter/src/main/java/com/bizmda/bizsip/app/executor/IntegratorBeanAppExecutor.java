package com.bizmda.bizsip.app.executor;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.IntegratorBeanInterface;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class IntegratorBeanAppExecutor extends AbstractAppExecutor {
    private IntegratorBeanInterface javaIntegratorService;
    private Class clazz;

    public IntegratorBeanAppExecutor(String serviceId, String type, Map configMap) {
        super(serviceId, type, configMap);
        String className = (String)configMap.get("className");
        try {
            this.clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("创建"+className+"类失败！",e);
            return;
        }
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doAppService(BizMessage<JSONObject> message) throws BizException {
        BizUtils.debug("入参",message);
        if (this.javaIntegratorService == null) {
            Object object = SpringUtil.getBean(this.clazz);
            if (object instanceof IntegratorBeanInterface) {
                this.javaIntegratorService = (IntegratorBeanInterface)object;
            }
            else {
                throw new BizException(BizResultEnum.OTHER_ERROR,"类["+this.clazz.getName()+"]不是BizServiceInterface接口实现类，创建BizService服务失败");
            }
        }
        log.debug("执行Java引擎:{}",this.javaIntegratorService.getClass().getName());
        BizMessage<JSONObject> result = this.javaIntegratorService.doBizService(message);
        BizUtils.debug("函数返回",result);
        return result;
    }
}
