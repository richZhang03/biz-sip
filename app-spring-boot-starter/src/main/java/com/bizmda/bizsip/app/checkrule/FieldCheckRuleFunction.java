package com.bizmda.bizsip.app.checkrule;

import cn.hutool.core.lang.Validator;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@Slf4j
public class FieldCheckRuleFunction {
    private FieldCheckRuleFunction() {

    }

    /**
     * 验证是否为身份证号码（支持18位、15位和港澳台的10位）
     */
    public static boolean isCitizenId(Object value, List<Object> args) {
        log.debug("isCitizenId({},{})", value, args);
        return Validator.isCitizenId((String) value);
    }

    public static boolean isEmail(Object value, List<Object> args) {
        log.debug("isEmail({},{})", value, args);
        return Validator.isEmail((String) value);
    }

    public static boolean notEmpty(Object value, List<Object> args) {
        log.debug("notEmpty({},{})", value, args);
        return !Validator.isEmpty(value);
    }

    /**
     * 通过正则表达式验证
     */
    public static boolean isMatchRegex(Object value, List<Object> args) {
        log.debug("isMatchRegex({},{})", value, args);
        return Validator.isMatchRegex((String) args.get(0), (String) value);
    }

    /**
     * 验证是否为金额，单位元2位小数
     */
    public static boolean isMoney(Object value, List<Object> args) {
        log.debug("isMoney({},{})", value, args);
        return Validator.isMatchRegex("([1-9]\\d*|0)(\\.\\d{1,2})?", (String) value);
    }

    /**
     * 验证是否为金额，单位分支持
     */
    public static boolean isCent(Object value, List<Object> args) {
        log.debug("isCent({},{})", value, args);
        return Validator.isMatchRegex("[1-9]\\d*", (String) value);
    }


    /**
     * 验证是否为URL
     */
    public static boolean isUrl(Object value, List<Object> args) {
        log.debug("isUrl({},{})", value, args);
        return Validator.isUrl((String) value);
    }

    /**
     * 验证该字符串是否是数字
     */
    public static boolean isNumber(Object value, List<Object> args) {
        log.debug("isNumber({},{})", value, args);
        return Validator.isNumber((String) value);
    }

    /**
     * 验证是否为手机号码（中国）
     */
    public static boolean isMobile(Object value, List<Object> args) {
        log.debug("isMobile({},{})", value, args);
        return Validator.isMobile((String) value);
    }

    /**
     * 验证给定的数字是否在指定范围内
     */
    public static boolean isBetween(Object value, List<Object> args) {
        log.debug("isBetween({},{})", value, args);
        return Validator.isBetween(Double.valueOf(value.toString()), Integer.valueOf(args.get(0).toString()), Integer.valueOf(args.get(1).toString()));
    }

    /**
     * 验证时间格式
     */
    public static boolean checkTimeFormatter(Object value, List<Object> args) {
        log.debug("checkTimeFormatter({},{})", value, args);
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern((String) args.get(0));
            // 格式验证
            LocalDateTime.parse((String) value, formatter);
        } catch (DateTimeParseException | NullPointerException e) {
            return false;
        }
        return true;
    }

    /**
     * 验证给定的字符传长度是否在指定范围内
     */
    public static boolean isLengthBetween(Object value, List<Object> args) {
        log.debug("isLengthBetween({},{})", value, args);
        int length = value.toString().length();
        if (length < Integer.valueOf((String) args.get(0))
                || length > Integer.valueOf((String) args.get(1))) {
            return false;
        }
        return true;
    }


}
