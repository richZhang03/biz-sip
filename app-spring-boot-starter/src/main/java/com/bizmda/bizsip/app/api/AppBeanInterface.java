package com.bizmda.bizsip.app.api;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;

/**
 * app-bean-service服务抽象接口
 */
public interface AppBeanInterface {
    /**
     * 执行聚合服务
     * @param message 传入的消息
     * @return 返回的消息
     */
    JSONObject process(JSONObject message) throws BizException;
}
