package com.bizmda.bizsip.app.executor;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.service.AppClientService;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class SinkServiceAppExecutor extends AbstractAppExecutor {
    private String sinkId;
    private AppClientService appClientService = null;

    public SinkServiceAppExecutor(String serviceId, String type, Map configMap) {
        super(serviceId, type, configMap);
        this.sinkId = (String)configMap.get("sinkId");
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doAppService(BizMessage<JSONObject> message) throws BizException {
        BizUtils.debug("入参",message);
        if (this.appClientService == null) {
            this.appClientService = SpringUtil.getBean(AppClientService.class);
        }
        BizMessage<JSONObject> result = this.appClientService.callSink(this.sinkId,message.getData());
        BizUtils.debug("函数返回",result);
        return result;
    }
}
