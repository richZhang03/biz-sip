package com.bizmda.bizsip.app.config;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.io.resource.NoResourceException;
import com.bizmda.bizsip.app.executor.*;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 史正烨
 */
@Slf4j
public class AppServiceMapping {
    private Map<String, AbstractAppExecutor> integratorExecutorMap;
    //    private Map<String, SpringBeanIntegratorExecutor> prefixServiceIntegratorExecutorMap;
    private String configPath;
//    private boolean isSpringBeanLoad = false;
//    private Map<String, JavaIntegratorServiceInterface> javaIntegratorServiceMap = null;

    private static final Map<String, Object> SERVICE_SCRIPT_SUFFIX_MAP = new HashMap<>();

    static {
        SERVICE_SCRIPT_SUFFIX_MAP.put("script", ScriptAppExecutor.class);
    }

    public AppServiceMapping(String configPath) throws BizException {
        this.configPath = configPath;
        this.loadAppServiceFromServiceDir();
        this.loadAppServiceFromServiceYml();
    }

    public void loadAppServiceFromServiceDir() throws BizException {
        String scriptPath = null;
        this.integratorExecutorMap = new HashMap<>(16);
        List<File> files = new ArrayList<>();
        if (this.configPath == null) {
            try {
                ClassPathResource resource = new ClassPathResource("/service");
                File fileDir = resource.getFile();
                scriptPath = fileDir.getPath();
                if (fileDir.exists() && fileDir.isDirectory()) {
                    files = FileUtil.loopFiles(fileDir);
                }
            } catch (NoResourceException e) {
                log.warn("资源为空:/service");
            }
        } else {
            if (this.configPath.endsWith("/")) {
                scriptPath = this.configPath + "service";
            } else {
                scriptPath = this.configPath + "/service";
            }
            files = FileUtil.loopFiles(scriptPath);
        }
        String suffix;
        AbstractAppExecutor appExecutor = null;

        for (File file : files) {
            suffix = FileUtil.getSuffix(file).toLowerCase();
            Class<AbstractAppExecutor> integratorClazz = (Class<AbstractAppExecutor>) SERVICE_SCRIPT_SUFFIX_MAP.get(suffix);
            if (integratorClazz != null) {
                FileReader fileReader = new FileReader(file);
                String fileContent = fileReader.readString();
                String allPath = FileUtil.normalize(file.getPath());
                String serviceId = allPath.substring(scriptPath.length(), allPath.length() - suffix.length() - 1);
                log.info("装载App服务:" + serviceId + "  类型:script");
                try {
                    Constructor<AbstractAppExecutor> constructor = integratorClazz.getDeclaredConstructor(String.class, String.class, Map.class);
                    Map configMap = new HashMap(16);
                    configMap.put("script", fileContent);
                    appExecutor = constructor.newInstance(serviceId, suffix, configMap);
                } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                    throw new BizException(BizResultEnum.INTEGRATOR_SERVICE_CLASS_LOAD_ERROR, e);
                }
                appExecutor.init();
                integratorExecutorMap.put(serviceId, appExecutor);
            }
        }
    }

    private void loadAppServiceFromServiceYml() throws BizException {
        Yaml yaml = new Yaml();
        List<Map<String, Object>> springbeanServiceList = null;
        try {
            if (this.configPath == null) {
                ClassPathResource resource = new ClassPathResource("/service.yml");
                springbeanServiceList = (List<Map<String, Object>>) yaml.load(new FileInputStream(resource.getFile()));
            } else {
                springbeanServiceList = (List<Map<String, Object>>) yaml.load(new FileInputStream(new File(this.configPath + "/service.yml")));
            }
        } catch (FileNotFoundException e) {
            throw new BizException(BizResultEnum.OTHER_FILE_NOTFOUND, "service.yml");
        }
        for (Map<String, Object> serviceConfigMap : springbeanServiceList) {
            String bizServiceId = (String) serviceConfigMap.get("bizServiceId");
            String type = (String) serviceConfigMap.get("type");
            log.info("装载App服务:" + bizServiceId + "  类型:" + type);
            switch (type) {
                case "bean-service":
                    BeanAppExecutor beanIntegratorExecutor = new BeanAppExecutor(bizServiceId, type, serviceConfigMap);
                    beanIntegratorExecutor.init();
                    this.integratorExecutorMap.put(bizServiceId, beanIntegratorExecutor);
                    break;
                case "app-bean-service":
                    AppBeanAppExecutor appBeanIntegratorExecutor = new AppBeanAppExecutor(bizServiceId, type, serviceConfigMap);
                    appBeanIntegratorExecutor.init();
                    this.integratorExecutorMap.put(bizServiceId, appBeanIntegratorExecutor);
                    break;
                case "integrator-bean-service":
                    IntegratorBeanAppExecutor integratorBeanIntegratorExecutor = new IntegratorBeanAppExecutor(bizServiceId, type, serviceConfigMap);
                    integratorBeanIntegratorExecutor.init();
                    this.integratorExecutorMap.put(bizServiceId, integratorBeanIntegratorExecutor);
                    break;
                case "sink-service":
                    SinkServiceAppExecutor serviceIntegratorExecutor = new SinkServiceAppExecutor(bizServiceId, type, serviceConfigMap);
                    serviceIntegratorExecutor.init();
                    this.integratorExecutorMap.put(bizServiceId, serviceIntegratorExecutor);
                    break;
                default:
                    log.error("装载出错，未知服务类型:" + type);
            }
        }
    }

    public AbstractAppExecutor getAppExecutor(String bizServiceId) {
//        if (!this.isSpringBeanLoad) {
//            this.isSpringBeanLoad = true;
////            this.loadBizServiceBeanService();
//            try {
//                this.loadAppServiceFromServiceYml();
//            } catch (BizException e) {
//                log.error("装载service.yml出错", e);
//            }
//        }

        AbstractAppExecutor abstractAppExecutor = this.integratorExecutorMap.get(bizServiceId);

        return abstractAppExecutor;
    }
}
