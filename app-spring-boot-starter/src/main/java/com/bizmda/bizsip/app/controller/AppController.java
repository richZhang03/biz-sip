package com.bizmda.bizsip.app.controller;

import cn.hutool.core.text.StrFormatter;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.checkrule.*;
import com.bizmda.bizsip.app.config.CheckRuleConfigMapping;
import com.bizmda.bizsip.common.*;
import com.bizmda.bizsip.app.config.AppServiceMapping;
import com.bizmda.bizsip.app.executor.AbstractAppExecutor;
import com.bizmda.bizsip.app.service.AppLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author 史正烨
 */
@Slf4j
@RestController
//@RequestMapping("/")
public class AppController {
    @Autowired
    private AppServiceMapping appServiceMapping;
    @Autowired
    private CheckRuleConfigMapping checkRuleConfigMapping;
    @Autowired
    private AppLogService appLogService;

    @PostMapping(value="/api",consumes = "application/json", produces = "application/json")
    public BizMessage<JSONObject> doApiService(HttpServletRequest request, HttpServletResponse response,
                                               @RequestBody JSONObject inJsonObject,
                                               @PathVariable(required = false) Map<String, Object> pathVariables,
                                               @RequestParam(required = false) Map<String, Object> parameters) throws BizException {
        String serviceId = request.getHeader("Biz-Service-Id");
        BizUtils.debug("integrator开始处理服务",serviceId);
        BizUtils.trace("请求消息",inJsonObject);
        BizMessage<JSONObject> inMessage = BizMessage.createNewTransaction();
        inMessage.setData(inJsonObject);
        BizUtils.bizMessageThreadLocal.set(inMessage);

        log.debug("校验域级规则");
        JSONArray jsonArray = this.checkFieldRule(serviceId,inJsonObject);
        if (!jsonArray.isEmpty()) {
            log.warn("域级校验出错:{}",jsonArray);
            throw new BizException(BizResultEnum.CHECKRULE_FIELD_CHECK_ERROR,jsonArray.toString());
        }

        log.debug("校验服务级规划");
        jsonArray = this.checkServiceRule(serviceId,inJsonObject);
        if (!jsonArray.isEmpty()) {
            log.warn("服务级检验出错{}",jsonArray);
            throw new BizException(BizResultEnum.CHECKRULE_SERVICE_CHECK_ERROR,jsonArray.toString());
        }

        AbstractAppExecutor appExecutor = this.appServiceMapping.getAppExecutor(serviceId);
        if (appExecutor == null) {
            throw new BizException(BizResultEnum.INTEGRATOR_SERVICE_NOT_FOUND,
                    StrFormatter.format("聚合服务不存在:{}",serviceId));
        }

        BizUtils.tmContextThreadLocal.set(new TmContext());
        BizUtils.serviceIdThreadLocal.set(serviceId);
        BizMessage<JSONObject> outMessage = null;
        log.debug("integrator执行聚合服务");
        try {
            outMessage = appExecutor.doAppService(inMessage);
        }
        catch (Exception e) {
            log.error("integrator执行出错",e);
            outMessage = BizMessage.buildFailMessage(inMessage,e);
        }
        finally {
            BizUtils.tmContextThreadLocal.remove();
            BizUtils.bizMessageThreadLocal.remove();
            BizUtils.serviceIdThreadLocal.remove();
        }
        if (outMessage.getCode() == 0) {
            log.debug("保存成功日志");
            this.appLogService.sendSuccessLog(serviceId,inMessage,outMessage);
        }
        else {
            log.debug("保存失败日志");
            this.appLogService.sendFailLog(serviceId,inMessage,outMessage);
        }
        return outMessage;
    }

    private JSONArray checkFieldRule(String serviceId, JSONObject message) throws BizException {
        BizUtils.debug("入参",serviceId,message);
        JSONArray jsonArray = new JSONArray();
        CheckRuleConfig checkRuleConfig = this.checkRuleConfigMapping.getCheckRuleConfig(serviceId);
        if (checkRuleConfig == null) {
            return jsonArray;
        }
        List<FieldCheckRule> fieldCheckRuleList = checkRuleConfig.getFieldCheckRuleList();
        if (fieldCheckRuleList == null) {
            return jsonArray;
        }
        List<FieldChcekRuleResult> fieldChcekRuleResultList = FieldCheckRuleHelper.checkFieldRule(message, fieldCheckRuleList,checkRuleConfig.getFieldCheckMode());

        for(FieldChcekRuleResult fieldChcekRuleResult:fieldChcekRuleResultList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.set("field", fieldChcekRuleResult.getField());
            jsonObject.set("message", fieldChcekRuleResult.getMessage());
            jsonArray.add(jsonObject);
        }
        BizUtils.debug("返回",jsonArray);
        return jsonArray;
    }

    private JSONArray checkServiceRule(String serviceId, JSONObject message) throws BizException {
        BizUtils.debug("入参",serviceId,message);
        JSONArray jsonArray = new JSONArray();
        CheckRuleConfig checkRuleConfig = this.checkRuleConfigMapping.getCheckRuleConfig(serviceId);
        if (checkRuleConfig == null) {
            return jsonArray;
        }
        List<ServiceCheckRule> serviceCheckRuleList = checkRuleConfig.getServiceCheckRuleList();
        if (serviceCheckRuleList == null) {
            return jsonArray;
        }
        List<ServiceChcekRuleResult> serviceChcekRuleResultList = ServiceCheckRuleHelper.checkServiceRule(message, serviceCheckRuleList,checkRuleConfig.getFieldCheckMode());

        for(ServiceChcekRuleResult serviceChcekRuleResult:serviceChcekRuleResultList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.set("message", serviceChcekRuleResult.getResult());
            jsonArray.add(jsonObject);
        }
        BizUtils.debug("返回",jsonArray);
        return jsonArray;
    }

}
