package com.bizmda.bizsip.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizConstant;
import com.bizmda.bizsip.common.BizMessage;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AppLogService {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Value("${bizsip.rabbitmq-log:false}")
    private boolean rabbitmqLog;

    public void sendSuccessLog(String serviceId, BizMessage<JSONObject> inBizMessage, BizMessage<JSONObject> outBizMessage) {
        if (!rabbitmqLog) {
            return;
        }
        Map<String,Object> map = new HashMap<>(4);
        map.put("type", BizConstant.SUCCESS_LOG_TYPE);
        map.put("serviceId", serviceId);
        map.put("request",inBizMessage);
        map.put("response",outBizMessage);
        rabbitTemplate.convertAndSend(BizConstant.BIZSIP_LOG_EXCHANGE, BizConstant.BIZSIP_LOG_ROUTING_KEY, map,
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) {
                        //设置消息持久化
                        message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
                        return message;
                    }
                });
    }

    public void sendSuspendLog(String serviceId, BizMessage<JSONObject> inBizMessage, BizMessage<JSONObject> outBizMessage) {
        if (!rabbitmqLog) {
            return;
        }
        Map<String,Object> map = new HashMap<>(4);
        map.put("type", BizConstant.SUSPEND_LOG_TYPE);
        map.put("serviceId", serviceId);
        map.put("request",inBizMessage);
        map.put("response",outBizMessage);
        rabbitTemplate.convertAndSend(BizConstant.BIZSIP_LOG_EXCHANGE, BizConstant.BIZSIP_LOG_ROUTING_KEY, map,
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) {
                        //设置消息持久化
                        message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
                        return message;
                    }
                });
    }

    public void sendFailLog(String serviceId, BizMessage<JSONObject> inBizMessage, BizMessage<JSONObject> outBizMessage) {
        if (!rabbitmqLog) {
            return;
        }
        Map<String,Object> map = new HashMap<>(4);
        map.put("type", BizConstant.FAIL_LOG_TYPE);
        map.put("serviceId", serviceId);
        map.put("request",inBizMessage);
        map.put("response",outBizMessage);
        rabbitTemplate.convertAndSend(BizConstant.BIZSIP_LOG_EXCHANGE, BizConstant.BIZSIP_LOG_ROUTING_KEY, map,
                new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) {
                        //设置消息持久化
                        message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
                        return message;
                    }
                });
    }

}
