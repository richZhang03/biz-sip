package com.bizmda.bizsip.config;

import java.util.Map;

/**
 * @author 史正烨
 */
public class RestSinkConfig extends AbstractSinkConfig {
    private String url;

    public String getUrl() {
        return url;
    }

    public RestSinkConfig(Map map) {
        super(map);
        this.setType("rest");
        this.url = (String)map.get("url");
    }
}
