package com.bizmda.bizsip.config;

import lombok.Data;

import java.util.*;

/**
 * @author 史正烨
 */
@Data
public class AbstractSinkConfig {
    private String id;
    private String type;
    private Map<String,Object> converterMap;
    private Map<String,Object> connectorMap;

    public AbstractSinkConfig(Map<String,Object> map) {
        this.id = (String)map.get("id");
        this.converterMap = (Map<String, Object>)map.get("converter");
        this.connectorMap = (Map<String, Object>)map.get("connector");
    }
}
