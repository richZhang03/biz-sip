package com.bizmda.bizsip.common;

import lombok.Getter;

/**
 * @author 史正烨
 */
@Getter
public class BizException extends Exception {
    private final int code;
    private final String extMessage;

    public BizException(ResultEnumInterface resultEnumInterface) {
        super((resultEnumInterface.getMessage()));
        this.code = resultEnumInterface.getCode();
        this.extMessage = null;
    }

    public BizException(ResultEnumInterface resultEnumInterface, Throwable e) {
        super(resultEnumInterface.getMessage(),e);
        this.code = resultEnumInterface.getCode();
        this.extMessage = null;
    }

    public BizException(int code,String message) {
        super(message);
        this.code = code;
        this.extMessage = null;
    }

    public BizException(BizResultEnum resultEnum, String extMessage) {
        super((resultEnum.getMessage()));
        this.code = resultEnum.getCode();
        this.extMessage = extMessage;
    }

    public BizException(BizResultEnum resultEnum, Throwable e, String extMessage) {
        super(resultEnum.getMessage(),e);
        this.code = resultEnum.getCode();
        this.extMessage = extMessage;
    }

    public BizException(int code,String message,String extMessage) {
        super(message);
        this.code = code;
        this.extMessage = extMessage;
    }

    public BizException(BizMessage bizMessage) {
        super(bizMessage.getMessage());
        this.code = bizMessage.getCode();
        this.extMessage = bizMessage.getExtMessage();
    }

    public boolean isTimeOutException() {
        return (this.code == BizResultEnum.INTEGRATOR_SERVICE_TIMEOUT.getCode());
    }
}
