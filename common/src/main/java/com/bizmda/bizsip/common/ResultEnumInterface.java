package com.bizmda.bizsip.common;

public interface ResultEnumInterface {
    public int getCode();
    public String getMessage();
}
