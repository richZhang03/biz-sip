package com.bizmda.bizsip.converter;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.config.CommonSourceConfig;
import com.bizmda.bizsip.config.SinkConfigMapping;
import com.bizmda.bizsip.config.SourceConfigMapping;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;

@Slf4j
public class Converter {
    private AbstractConverter converter;
//    private List<PredicateRuleConfig> serviceRules = null;

    public static Converter getSourceConverter(String sourceId) {
        Converter converter = new Converter();
        SourceConfigMapping sourceConfigMapping = SpringUtil.getBean(SourceConfigMapping.class);
        CommonSourceConfig sourceConfig = sourceConfigMapping.getSourceId(sourceId);
        try {
            if (sourceConfig == null) {
                throw new BizException(BizResultEnum.SOURCE_ID_NOTFOUND, "source:" + sourceId);
            }
            String converterType = (String) sourceConfig.getConverterMap().get("type");
            Class<? extends AbstractConverter> clazz = (Class<? extends AbstractConverter>) AbstractConverter.CONVERTER_TYPE_MAP.get(converterType);
            if (clazz == null) {
                throw new BizException(BizResultEnum.CONVERTOR_NOT_SET);
            }

            try {
                converter.converter = clazz.getDeclaredConstructor().newInstance();
            } catch (InstantiationException | IllegalAccessException
                    | InvocationTargetException | NoSuchMethodException e) {
                throw new BizException(BizResultEnum.CONVERTOR_CREATE_ERROR, e);
            }
            converter.converter.init(sourceConfigMapping.getConfigPath(), sourceConfig.getConverterMap());
        } catch (BizException e) {
            log.error("getSourceConverter("+sourceId+")出错!",e);
            return null;
        }
        return converter;
    }

    public static Converter getSinkConverter(String sinkId) {
        Converter converter = new Converter();
        SinkConfigMapping sinkConfigMapping = SpringUtil.getBean(SinkConfigMapping.class);
        AbstractSinkConfig sinkConfig = sinkConfigMapping.getSinkConfig(sinkId);
        try {
            if (sinkConfig == null) {
                throw new BizException(BizResultEnum.SINK_NOT_SET, "sinkId[" + sinkId + "]在sink.yml中没有配置");
            }
            if (sinkConfig.getConverterMap() == null) {
                converter.converter = null;
                return converter;
            }
            String converterType = (String) sinkConfig.getConverterMap().get("type");
            Class<Object> clazz = (Class<Object>) AbstractConverter.CONVERTER_TYPE_MAP.get(converterType);
            if (clazz == null) {
                throw new BizException(BizResultEnum.CONVERTOR_NOT_SET);
            }
            try {
                converter.converter = (AbstractConverter) clazz.getDeclaredConstructor().newInstance();
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                throw new BizException(BizResultEnum.CONVERTOR_CREATE_ERROR, e);
            }
            converter.converter.init(sinkConfigMapping.getConfigPath(), sinkConfig.getConverterMap());
        }
        catch (BizException e) {
            log.error("getSinkConverter("+sinkId+")出错!",e);
            return null;
        }
        return converter;
    }

    public AbstractConverter getConverter() {
        return this.converter;
    }

    public JSONObject unpack(byte[] inMessage) throws BizException {
        return this.converter.unpack(inMessage);
    }

    public byte[] pack(JSONObject inMessage) throws BizException {
        return this.converter.pack(inMessage);
    }

//    public String getServiceIdByServicePredicateRule(JSONObject inData) throws BizException {
//        if (this.serviceRules == null) {
//            throw new BizException(BizResultEnum.OTHER_NO_MATCH_SERVICE_RULE);
//        }
//        for (PredicateRuleConfig predicateRuleConfig : this.serviceRules) {
//            if (predicateRuleConfig.getPredicate() == null ||
//                    predicateRuleConfig.getPredicate().isEmpty()) {
//                return BizUtils.getElStringResult(predicateRuleConfig.getRule(), inData);
//            }
//            boolean predicateFlag = BizUtils.getElBooleanResult(predicateRuleConfig.getPredicate(), inData);
//            if (predicateFlag) {
//                return BizUtils.getElStringResult(predicateRuleConfig.getRule(), inData);
//            }
//        }
//        throw new BizException(BizResultEnum.OTHER_NO_MATCH_SERVICE_RULE);
//    }
}
