package com.bizmda.bizsip.sample.source.controller;

import cn.hutool.core.util.HexUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.bizmda.bizsip.source.client.SourceClientFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;

/**
 * @author 史正烨
 */
@Slf4j
@RestController
public class SampleSource3Controller {
    private BizMessageInterface appServiceClient = SourceClientFactory
            .getAppServiceClient(BizMessageInterface.class,"/sink13");

    @GetMapping(value ="/sink13")
    public BizMessage<JSONObject> doService1() throws BizException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("msgType","0800");
        String msgHead = "1234567890123456789012345678901234567890123456";
        jsonObject.set("msgHead",HexUtil.encodeHexStr(msgHead));
        jsonObject.set("systemTraceAuditNumber11","000001");
        jsonObject.set("card_accptr_termnl_id41",new String(HexUtil.decodeHex("3733373832323134"),StandardCharsets.ISO_8859_1));
        jsonObject.set("card_accptr_id42", new String(HexUtil.decodeHex("383938343131333431333130303134"), StandardCharsets.ISO_8859_1));
        jsonObject.set("reserved60","000000000030");
        jsonObject.set("switching_data62","53657175656e6365204e6f3132333036303733373832323134");
        jsonObject.set("fin_net_data63","303031");
        jsonObject.set("acct_id_1_102","1234567890");
        return this.appServiceClient.call(jsonObject);
    }
}