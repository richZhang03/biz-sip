package com.bizmda.bizsip.sample.sink.controller;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.sink.connector.sinkbean.SinkBeanInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 史正烨
 */
@Slf4j
@Service
public class EchoBeanServer implements SinkBeanInterface {
    @Override
    public byte[] process(byte[] inMessage) throws BizException {
        log.debug("EchoBeanServer传入消息:\n{}", BizUtils.getString(inMessage));
        log.debug("EchoBeanServer传入消息:\n{}", BizUtils.buildHexLog(inMessage));
        return inMessage;
    }
}
