package com.bizmda.bizsip.sample.app;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.app.client.AppClientFactory;
import com.bizmda.bizsip.app.service.AppClientService;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IntegratorService4 implements AppBeanInterface {
    @Autowired
    private AppClientService appClientService;
    private BizMessageInterface bizMessageInterface
            = AppClientFactory.getDelayAppServiceClient(
            BizMessageInterface.class, "/sample/delayservice1",
            1000, 2000, 4000, 8000, 16000, 32000);


    @Override
    public JSONObject process(JSONObject message) throws BizException {
        BizMessage<JSONObject> bizMessage = this.bizMessageInterface.call(message);
        return bizMessage.getData();
    }
}
