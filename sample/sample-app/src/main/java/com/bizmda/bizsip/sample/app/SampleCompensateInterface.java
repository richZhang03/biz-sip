package com.bizmda.bizsip.sample.app;

import com.bizmda.bizsip.common.BizException;

public interface SampleCompensateInterface {
    String tx01(String tranId,String data) throws BizException;
    void forwardCompensateTx01(String tranId,String data) throws BizException;
    String tx02(String tranId,String data) throws BizException;
    void backwardCompensateTx02(String tranId,String data) throws BizException;
}
