package com.bizmda.bizsip.sample.sink.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDTO {
    private String customerId;
    private String name;
    private Character sex;
    private Integer age;
    private Boolean married;
}
