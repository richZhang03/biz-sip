package com.bizmda.bizsip.source.service;

import com.bizmda.bizsip.common.BizException;

/**
 * Source服务接口
 */
public interface SourceServiceInterface {
    /**
     *
     * @param data 传入Source服务的数据
     * @return Source服务返回数据
     * @throws BizException
     */
    Object doService(Object data) throws BizException;
}
