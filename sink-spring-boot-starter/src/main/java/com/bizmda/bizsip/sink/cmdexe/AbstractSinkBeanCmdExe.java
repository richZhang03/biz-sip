package com.bizmda.bizsip.sink.cmdexe;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;

abstract public class AbstractSinkBeanCmdExe {
    /**
     * JSONObject SinkBean服务调用接口
     * @param inJsonObject 传入的消息
     * @return 返回值
     * @throws BizException
     */
    abstract public JSONObject execute(JSONObject inJsonObject) throws BizException;
}
