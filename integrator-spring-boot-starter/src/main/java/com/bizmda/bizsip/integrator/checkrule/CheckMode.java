package com.bizmda.bizsip.integrator.checkrule;

public enum  CheckMode {
    // 返回首次校验失败
    ONE,
    // 返回所有校验失败结果
    ALL
}
