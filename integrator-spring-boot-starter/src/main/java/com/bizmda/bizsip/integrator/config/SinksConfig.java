package com.bizmda.bizsip.integrator.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 史正烨
 */
@Slf4j
@Component
@Data
@ConfigurationProperties(prefix = "bizsip")
public class SinksConfig {
    private List<Map<String,Object>> sinks;
    private Map<String,Map<String,Object>> sinkMap = null;

    public Map<String,Object> getSinkConfig(String id) {
        if (this.sinkMap == null) {
            this.setupSinkMap();
        }
        return this.sinkMap.get(id);
    }

    private void setupSinkMap() {
        log.info("setupSinkMap:{}",this.sinks);
        this.sinkMap = new HashMap<>(16);
        for (Map<String,Object> map:this.sinks) {
            this.sinkMap.put((String)map.get("id"),map);
        }
    }
}
