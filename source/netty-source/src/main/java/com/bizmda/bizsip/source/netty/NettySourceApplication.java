package com.bizmda.bizsip.source.netty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@SpringBootApplication
@ComponentScan(basePackages={"cn.hutool.extra.spring","com.bizmda.bizsip.source.netty"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class NettySourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(NettySourceApplication.class, args);
    }
}
